(!code:start)
function(jtsTracker,oGateConfig){

	var fOldPush = function (){};

	var self = this;
	this.globalTimeout = false;
	this.oConf = oGateConfig;
	this.jtsTracker = jtsTracker;
	this.oNamespace = {};
	this.bSubmit = false;

	this._construct = function(){

		// Call Function from Page-Change

		this.oConf.pagefunction.apply(this.generateFunctionScope(this),[this.oNamespace]);


		this.iIntervalCounter = 0;
		this.intervalID = window.setInterval(function() {
			if(
				typeof window[self.oConf.datalayer] !== "undefined"
			) {
				window.clearInterval(self.intervalID);

				if(Array.isArray(window[self.oConf.datalayer])) {
					//Falls schon etwas im Array steht, dieses durchlaufen und an die Basis Funktion übergeben.
					for(var iArray=0;iArray < window[self.oConf.datalayer].length;iArray++)
					{
						self.fBaseTrackingFunction.call(self,window[self.oConf.datalayer][iArray]);
					}
				}

				//Nun kann das Array ohne Problem überschrieben werden mit einem Objekt und einer MEthode mit PUSH.
				fOldPush = window[self.oConf.datalayer].push;
				window[self.oConf.datalayer].push	= function(arg)
				{
					self.fBaseTrackingFunction.call(self, arg);
					return fOldPush.call(window[self.oConf.datalayer], arg);
				};

			} else {
				self.iIntervalCounter++;

				if(self.iIntervalCounter > 4) {
					window.clearInterval(self.intervalID);
				}
			}
		},500);
	};

	this.fBaseTrackingFunction = function()
	{
		// This variable stores the original datalayer object
		var aOriginalArg = arguments;

		//Wenn es sich bei Type um Array handelt, ist es ein Objekt an der Stelle 0, daher wird jetzt die Stelle 0 weggenommen.
		aOriginalArg = aOriginalArg[0];

		this.oConf.codefunction.apply(this.generateFunctionScope(this),[aOriginalArg, this.oNamespace]);


	};

	this.generateFunctionScope = function(scope) {
		return {
			checkObject : function(object,aSplit,counter) {
				if(typeof counter === "undefined") {
					counter = 0;
				}

				if(aSplit.length === counter)
				{
					return object;
				}
				else
				{
					if(typeof object[aSplit[counter]] !== "undefined")
					{
						return this.checkObject(object[aSplit[counter]],aSplit,++counter);
					}
					else
					{
						return null;
					}
				}
			},
			push : function(object) {
				scope.jtsTracker.track(object);
			},
			trackSubmit : function() {
				if(scope.bSubmit === false) {
					scope.jtsTracker.track({"track":"submit"});
					scope.bSubmit = true;
				}
			}
		};
	};

	this._construct();
}
(!code:end)